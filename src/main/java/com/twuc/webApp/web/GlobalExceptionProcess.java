package com.twuc.webApp.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;

@ControllerAdvice
public class GlobalExceptionProcess {

    private static Logger log = LoggerFactory.getLogger(GlobalExceptionProcess.class);

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<HashMap<String, String>> processException(IllegalArgumentException exception) {
        log.error(exception.getMessage());
        HashMap<String, String> exceptionMap = new HashMap<>();
        exceptionMap.put("message", exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(exceptionMap);
    }
}
