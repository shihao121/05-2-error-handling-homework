package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class MazeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_message_when_given_correct_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/color-solve"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_400_with_error_message_use_global_exception_handler() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/error-type"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}