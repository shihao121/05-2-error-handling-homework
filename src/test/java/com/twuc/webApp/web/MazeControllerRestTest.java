package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class MazeControllerRestTest {
    
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_400_with_error_message_by_global_exception_handler() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/mazes/error-type", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, forEntity.getStatusCode());
        assertEquals("{\"message\":\"Invalid type: error-type\"}", forEntity.getBody());
    }
}
